## 1.0.20 (2024-06-18)

### added (1 change)

- [no message](https://gitlab.com/mekert1/releases-gitlab/-/commit/53addaece9ccda2d9e27a65b69c416ea3ca71dde) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/23))

## 1.0.19 (2024-06-18)

### added (1 change)

- [Testing out stage re-ordering](https://gitlab.com/mekert1/releases-gitlab/-/commit/44ae0bee6d5ef2cec9d6a8e63f840b32cb797d73) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/22))

## 1.0.18 (2024-06-18)

No changes.

## 1.0.17 (2024-06-18)

### added (1 change)

- [Updating tags and variables](https://gitlab.com/mekert1/releases-gitlab/-/commit/7b7b285851baf6d557b98371c1496d7630b63718) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/21))

## 1.0.15 (2024-06-18)

### added (1 change)

- [no message](https://gitlab.com/mekert1/releases-gitlab/-/commit/289d770d0a159f0f188fdf1d593f9003856618ca) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/19))

## 1.0.14 (2024-06-18)

### added (1 change)

- [Update](https://gitlab.com/mekert1/releases-gitlab/-/commit/1ddb6c9365dd4d50cf4a859554886fe8baeaf7e4) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/18))

## 1.0.7 (2024-06-14)

### added (1 change)

- [AE-XXXX - Changing label idk sure](https://gitlab.com/mekert1/releases-gitlab/-/commit/f2922551f038fc7eb0b7fee4c934e2cba27915b1) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/11))

## 1.0.6 (2024-06-14)

No changes.

## 1.0.5 (2024-06-14)

### added (1 change)

- [Adding button content to shared widget](https://gitlab.com/mekert1/releases-gitlab/-/commit/e7a698c80ae28eb1769562be4fce999758642942) ([merge request](https://gitlab.com/mekert1/releases-gitlab/-/merge_requests/9))
