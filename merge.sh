#!/bin/bash
get_merge_requests() {
  local green="\033[1;32m"
  local blue="\033[1;34m"
  local yellow="\033[1;33m"
  local red="\033[1;31m"
  local reset="\033[0m"
  local checkBox="\xE2\x9C\x94"
  local crossMark="\xE2\x9C\x98"
  local bullet="\xE2\x80\xA2"

	ACCESS_TOKEN="glpat-ySa81j7z8ap2FNosnsUT"
	PROJECT_ID="58869975"
	VERSION="1.0.0"
  dateISO8601=$(TZ='America/New_York' date --date='24 hours ago -5 hours' +'%Y-%m-%dT%H:%M:%S')

	# Get a response back checking for MRs made today
  MR_RESP=$(curl -s --location "gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests?approved=yes&created_after=$dateISO8601&state=merged&target_branch=develop" \
  --header "PRIVATE-TOKEN: $ACCESS_TOKEN")

	if [[ "$MR_RESP" = "[]" ]]; then
    echo -e "${red}${crossMark}${reset} - No merge requests found within the specified criteria"
		exit 1  # Exit with failure, skip daily pipeline
	else
	  git config --global user.email "denali_ci@anywhere.re"
	  git config --global user.name "Denali CI"
	  git fetch origin
	  # checkout the release branch for the current version, release/#.#.#
	  git checkout -B "release/$VERSION"
	  git branch --set-upstream-to=origin/release/$VERSION release/$VERSION

    NUMBER_OF_ENTRIES=$(echo "$MR_RESP" | jq 'length')
    echo -e "${green}${checkBox}${reset} - $NUMBER_OF_ENTRIES Merge request(s) found"

    # Parse the JSON and use the first merge request
    MR=$(echo "$MR_RESP" | jq -c '.[0]')

    # cherry pick MR into the branch and commit
    MR_SHA=$(echo "$MR" | jq -r '.sha')
    MR_TITLE=$(echo "$MR" | jq -r '.title')
    git cherry-pick -m 1 --no-commit "$MR_SHA"
    COMMIT_MESSAGE="Cherry-pick: $MR_TITLE ($MR_SHA) [ci skip]"
    git commit -m "$COMMIT_MESSAGE"

    # Push the new changes to the current release branch
    git push
	fi
}